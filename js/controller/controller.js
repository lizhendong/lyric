var api = angular.module("app", ['ngTouch']);
api.controller("index", ["$scope", "$http",
	function($scope, $http) {
		var ls = window.localStorage;
		$scope.layershow = false;
		$scope.nexticon = false;
		$scope.content = [];
		$scope.i = 0;
		$scope.likeToggle = false;
		function getData(f) {
			$http.get('test/content.json', {
					params: {
						"art": "next"
					}
				})
				.success(function(data) {
					$scope.art = data.response;
					if (f) {
						$scope.nextthis();
					}
				});
		}
		getData('1');
		$scope.nextthis = function() {
			//			$http.get('test/content.json', {
			//					params: {
			//						"art": "next"
			//					}
			//				})
			//			.success(function (data) {
			//				$scope.art = data.response;
			//				$scope.content = data.response.content.split(';');
			//			})
			if (!$scope.likeToggle) {
				getData();
				$scope.i = Math.floor(Math.random() * 80);
				$scope.content = $scope.art[$scope.i].content.split(';');
				$scope.title = $scope.art[$scope.i].title;
				if (ls.like) {
					var likestr = ls.like,
						likearr = likestr.split(",");
					if ('0' == likearr[$scope.i]) {
						$scope.like = false;
					} else {
						$scope.like = true;
					}
				}
			} else {
				var j = $scope.art.length,
					i = Math.floor(Math.random() * j);
				$scope.content = $scope.art[i].content.split(';');
				$scope.title = $scope.art[i].title;
				$scope.like = true;
			}
		};
		$scope.likethis = function() {
			//			$http.get('test/mock.json',{
			//				params:{
			//					"like":"10001"
			//				}
			//			})
			//			.success(function (data) {
			//				if (data.status.code) {
			//					$scope.art.like = !$scope.art.like;
			//				}
			//			})
			if (!$scope.likeToggle) {
				$scope.art[$scope.i].like = !$scope.art[$scope.i].like;
				$scope.like = $scope.art[$scope.i].like;
				if (ls.like) {
					var likestr = ls.like,
						likearr = likestr.split(",");
					if ('0' == likearr[$scope.i]) {
						likearr[$scope.i] = 1;
					} else {
						likearr[$scope.i] = 0;
					}
					ls.like = likearr;
				} else {
					var likearr = [];
					for (var i = 0; i < $scope.art.length; i++) {
						likearr[i] = 0;
					}
					likearr[$scope.i] = 1;
					ls.setItem('like', likearr);
				}
			} else {
				plus.nativeUI.alert("请先取消LIKE浏览模式", function() {
					console.log("开发中");
				}, "lyric提示", "OK");
			}
		}
		$scope.alert = function() {
			plus.nativeUI.alert("开发中,请联系作者获取最新版本!", function() {
				console.log("开发中");
			}, "lyric提示", "OK");
		}
		$scope.likeGroup = function() {
			if (ls) {
				var likestr = ls.like,
					likearr = likestr.split(","),
					group = [];
				for (var i = 0, j = 0; i < likearr.length; i++) {
					if ('1' == likearr[i]) {
						group[j] = $scope.art[i];
						j++;
					}
				}
				$scope.art = group;
				$scope.likeToggle = !$scope.likeToggle;
			}
		}
		
	}
]);